import cv2 
import numpy as np 
import mediapipe as mp 
import HandModule as htm
import autopy  
import time


# Get tip of fingers 
# Check which fingers are up 
# Only Index finger in moving (Change coordinates to display)
# Smoothen the values 
# Move mouse 
# Specify Clicking mode (fingers 1 & 2)
# if distance < x then click  


cap = cv2.VideoCapture(0)

pTime = 0
cTime = 0

detector = htm.handDetector(maxHands=1)

#Screen size
wScr, hScr = autopy.screen.size()
smoothness = 15

plocX, plocY = 0, 0
clocX, clocY = 0, 0

# Window parameters
wCam , hCam = 640 , 480
cap.set(3, wCam)
cap.set(4, hCam)
frameR = 50 # frame reduction
#print(wScr, hScr)

while True: 
    success, img = cap.read()
    img = detector.findHands(img)
    lmList, bbox = detector.findPositions(img)

    if len(lmList) !=0:
        x1, y1 = lmList[8][1:]
        x2, y2 = lmList[12][1:]
        #print(x1, y1, x2, y2)

        fingers = detector.fingersUp()
        #print(fingers)

        cv2.rectangle(img, (frameR, frameR), (wCam-frameR, hCam-frameR), (255,0,255), 2)

        if fingers[1] == 1 and fingers[2] ==0:
            # Convert Coordinates
            x3 = np.interp(x1, (frameR, wCam-frameR), (0, wScr))
            y3 = np.interp(y1, (frameR, hCam-frameR), (0, hScr))
            
            # Smoothening mouse 
            clocX = plocX + (x3 - plocX) / smoothness
            clocY = plocY + (y3 - plocY) / smoothness


            autopy.mouse.move(wScr - clocX, clocY)
            cv2.circle(img, (x1, y1), 10, (255,0,255), cv2.FILLED) # Circle while moving the mouse 
            
            plocX, plocY = clocX, clocY
            

        if fingers[1]==1 and fingers[2]==1:
            length, img, lineInfo = detector.findDistance(8, 12, img)
            
            # Click if distance is short  
            if length < 35: 
                cv2.circle(img, (lineInfo[4], lineInfo[5]), 15, (0, 255, 0), cv2.FILLED)
                autopy.mouse.click()


    cTime = time.time()
    fps = 1/ (cTime - pTime)
    pTime = cTime

    # Fps Text
    cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_DUPLEX, 3, (255,0,255), 3 )

    # Display Camera window
    cv2.imshow("Image", img)
    cv2.waitKey(1)