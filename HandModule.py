import cv2
import time
import math 
import numpy as np 
import mediapipe as mp


class handDetector():
    def __init__(self, mode=False, maxHands = 2, detectionCon = 0.5, trackCon = 0.5 ):
        self.mode = mode
        self.maxHands = maxHands
        self.detectionCon = detectionCon
        self.trackCon = trackCon

        self.mpHands = mp.solutions.hands
        self.hands = self.mpHands.Hands(self.mode, self.maxHands, self.detectionCon, self.trackCon)
        self.mpDraw = mp.solutions.drawing_utils
        self.tipIds = [4, 8, 12, 16, 20]


    def findHands(self, img, draw=True):
        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.results = self.hands.process(imgRGB)
        #print(results.multi_hand_landmarks)

        if self.results.multi_hand_landmarks: 
            for handLms in self.results.multi_hand_landmarks:
                if draw:
                    self.mpDraw.draw_landmarks(img, handLms, self.mpHands.HAND_CONNECTIONS)

               
        return img



    def findPositions(self, img, handNo = 0, draw=True):
        self.lmList= []
        yList = []
        xList = []
        bbox = [] 

        if self.results.multi_hand_landmarks:
            myHand = self.results.multi_hand_landmarks[handNo]

            for id, lm in enumerate(myHand.landmark):
                #print(id, lm)
                h, w, c = img.shape
                cx, cy = int(lm.x*w), int(lm.y*h)
                #print(id, cx, cy)
               
                self.lmList.append([id, cx, cy])
                yList.append(cy)
                xList.append(cx)

                if draw:
                    cv2.circle(img, (cx, cy), 5, (255, 0, 255), cv2.FILLED )
        
        if len(xList) !=0 and len(yList) != 0:
             xmin, xmax = min(xList), max(xList)
             ymin, ymax = min(yList), max(yList)
             bbox = xmin, ymin, xmax, ymax
        
        return self.lmList, bbox


    # No. of fingers in the camera window
    def fingersUp(self):
        fingers = []
       
        if len(self.lmList) != 0:
            # Thumb
            if self.lmList[self.tipIds[0]][1] > self.lmList[self.tipIds[0] - 1][1]:
                fingers.append(1)
            else:
                fingers.append(0)

            # Fingers
            for id in range(1, 5):
                if self.lmList[self.tipIds[id]][2] < self.lmList[self.tipIds[id] - 2][2]:
                    fingers.append(1)
                else:
                    fingers.append(0)

        # totalFingers = fingers.count(1)
        return fingers       

    # Distance between 2 fingers
    def findDistance(self, p1, p2, img, draw=True,r=5, t=3):
        length = 0
        x1= 0
        y1= 0
        x2 = 0
        y2 = 0
        cx = 0
        cy = 0
        
        if len(self.lmList) != 0:
            x1, y1 = self.lmList[p1][1:]
            x2, y2 = self.lmList[p2][1:]
            cx, cy = (x1 + x2) // 2, (y1 + y2) // 2

            if draw:
                cv2.line(img, (x1, y1), (x2, y2), (255, 0, 255), t)
                cv2.circle(img, (x1, y1), r, (255, 0, 255), cv2.FILLED)
                cv2.circle(img, (x2, y2), r, (255, 0, 255), cv2.FILLED)
                cv2.circle(img, (cx, cy), r, (0, 0, 255), cv2.FILLED)
                length = math.hypot(x2 - x1, y2 - y1)
        

        return length, img, [x1, y1, x2, y2, cx, cy]

    
    

def main():
    pTime = 0
    cTime = 0
    cap = cv2.VideoCapture(0)
        
    # Window parameters
    #wCam , hCam = 480 , 360
    #cap.set(3, wCam)
    #cap.set(4, hCam)

    detector = handDetector()

    while True: 
        success, img = cap.read()
        img = detector.findHands(img)
        lmList, bbox = detector.findPositions(img)
        fingers = detector.fingersUp()
        distance = detector.findDistance(4,8, img)
        
        
        cTime = time.time()
        fps = 1/ (cTime - pTime)
        pTime = cTime

        # Fps Text
        cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_DUPLEX, 3, (255,0,255), 3 )

        # Display Camera window
        cv2.imshow("Image", img)
        cv2.waitKey(1)

if __name__ == "__main__":
    main()